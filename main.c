/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support  -  ROUSSET  -
 * ----------------------------------------------------------------------------
 * Copyright (c) 2006, Atmel Corporation

 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 * File Name           : main.c
 * Object              :
 * Creation            : ODi Apr 19th 2006
 *-----------------------------------------------------------------------------
 */
#include "include/part.h"
#include "include/main.h"
#include "include/debug.h"
#include "include/dataflash.h"
#include "include/nandflash.h"
#include "include/norflash.h"

#define NULL ((void*)0)
typedef unsigned long uint32_t;
typedef unsigned char uint8_t;

static inline uint32_t bswap32(uint32_t v)
{
	uint32_t r=0;
	r |= (0x000000ff & (v >> 24));
	r |= (0xff000000 & (v << 24));
	r |= (0x00ff0000 & (v << 8));
	r |= (0x0000ff00 & (v >> 8));
	return r;
}

#define IH_MAGIC        0x27051956      /* Image Magic Number           */
#define IH_NMLEN                32      /* Image Name Length            */

// U-Boot Image header
typedef struct image_header {
	uint32_t    ih_magic;   /* Image Header Magic Number  */
	uint32_t    ih_hcrc;    /* Image Header CRC Checksum  */
	uint32_t    ih_time;    /* Image Creation Timestamp   */
	uint32_t    ih_size;    /* Image Data Size            */
	uint32_t    ih_load;    /* Data  Load  Address        */
	uint32_t    ih_ep;      /* Entry Point Address        */
	uint32_t    ih_dcrc;    /* Image Data CRC Checksum    */
	uint8_t     ih_os;      /* Operating System        */
	uint8_t     ih_arch;    /* CPU architecture        */
	uint8_t     ih_type;    /* Image Type            */
	uint8_t     ih_comp;    /* Compression Type        */
	uint8_t     ih_name[IH_NMLEN];    /* Image Name        */
} image_header_t;

static void load_data(uint32_t src,uint32_t size,void* addr)
{
	
/* ==================== 2nd step: Load from media ==================== */
	/* Load from Dataflash in RAM */
#ifdef CFG_DATAFLASH
	load_df(AT91C_SPI_PCS_DATAFLASH, src, size,(uint32_t) addr);
#endif

	/* Load from Nandflash in RAM */
#ifdef CFG_NANDFLASH
	load_nandflash(src, size, (uint32_t) addr);
#endif

	/* Load from Norflash in RAM */
#ifdef CFG_NORFLASH
	load_norflash(src, size, (uint32_t) addr);
#endif
}

static void* load_zImage(uint32_t src,void* addr)
{
	// Load the header
	// 0x24 magic
	// 0x28 start
	// 0x2C end
	uint32_t header[3];
	load_data(src + 0x24,12,&header);
	// check magic number
	if(header[0] == 0x016f2818)
	{
		uint32_t size = header[2] - header[1];
		// Load the full image
		load_data(src,size,addr);
	}
	else
	{
#ifdef CFG_DEBUG
		dbg_print("Bad zImage magic");
#endif
		return NULL;
	}
	return addr;
}

static void* load_uImage(uint32_t src,void* addr)
{
	// Load the uImage header
	image_header_t header;
	load_data(src,sizeof(header),&header);
	// The data in the header is stored in BE
#ifdef CFG_DEBUG
	// Check the magic value
	uint32_t magic = bswap32(header.ih_magic);
	if(IH_MAGIC != magic)
	{
		dbg_print("Invalid uImage magic number");
		return NULL;
	}
#endif
	// TODO: check the header crc
	// TODO: check the compression type
	// TODO: check the CPU arch
	uint32_t size  = bswap32(header.ih_size);
	uint32_t load  = bswap32(header.ih_load);
	uint32_t entry = bswap32(header.ih_ep);
	// Load the image payload to the destination address
	load_data(src + sizeof(header),size,(void*)load);
	// TODO: check the image data crc	
	/* Jump to the Image Address */
	return (void*)entry;
}

/*------------------------------------------------------------------------------*/
/* Function Name       : main							*/
/* Object              : Main function						*/
/* Input Parameters    : none							*/
/* Output Parameters   : True							*/
/*------------------------------------------------------------------------------*/
int main(void)
{
/* ================== 1st step: :Hardware Initialization ================= */
	/* Performs the hardware initialization */
#ifdef CFG_HW_INIT
	hw_init();
#endif
#define ATAG_ADDRESS  0xB580
#define ATAG_SIZE     IMG_ADDRESS - ATAG_ADDRESS
	// Load the ATAGS
	load_data(ATAG_ADDRESS,ATAG_SIZE,(void*)0x20000100);
	void (*entry)(uint32_t,uint32_t,uint32_t) = NULL;
	entry = load_zImage(IMG_ADDRESS,(void*)0x23b00000);
	if(entry)
	{
		// point of return!
		entry(0,MACH_TYPE,0x20000100);
	}
	return 0;
}

